
interface PokemonCenter {
    fun givePokemon()
    fun takePokemon()
}

interface PokemonCenterDecorator : PokemonCenter

class BasicPokemonCenterService : PokemonCenter {
    override fun givePokemon() = println("Thanks for wait, you can take back your pokemon")
    override fun takePokemon() = println("I'll take your Pokemon")
}

class HealingPokemon(private val pokemonCenterService: PokemonCenter) : PokemonCenterDecorator {
    override fun givePokemon() {
        pokemonCenterService.givePokemon()
        println("Pokemon Cured")
    }

    override fun takePokemon() {
        pokemonCenterService.takePokemon()
        println("Healing Pokemon")
    }
}

class DepositWithdrawPokemon(private val pokemonCenterService: PokemonCenter) : PokemonCenterDecorator {
    override fun givePokemon() {
        pokemonCenterService.givePokemon()
        println("You take back a pokemon for Bill PC")
    }

    override fun takePokemon() {
        pokemonCenterService.takePokemon()
        println("You put a pokemon inside Bill PC")
    }
}

class PokemonExchange(private val pokemonCenterService: PokemonCenter) : PokemonCenterDecorator {
    override fun givePokemon() {
        pokemonCenterService.givePokemon()
        println("You have a new Pokemon")
    }

    override fun takePokemon() {
        pokemonCenterService.takePokemon()
        println("The pokemon has been exchanging")
    }
}

fun main(args: Array<String>) {
    //EL VALOR DETERMINA QUE SERVICIO SE VA A OCUPAR EN EL CENTRO POKEMON
    val clientAction = 2
    when(clientAction){
        1 -> {
            val pokemonCenterService = HealingPokemon(BasicPokemonCenterService())
            pokemonCenterService.takePokemon()
            pokemonCenterService.givePokemon()
        }

        2 -> {
            val pokemonCenterService = DepositWithdrawPokemon(BasicPokemonCenterService())
            pokemonCenterService.takePokemon()
            pokemonCenterService.givePokemon()
        }

        3 -> {
            val pokemonCenterService = PokemonExchange(BasicPokemonCenterService())
            pokemonCenterService.takePokemon()
            pokemonCenterService.givePokemon()
        }

    }

}